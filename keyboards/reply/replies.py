import telebot
from telebot import types
from telebot.types import (
    ReplyKeyboardMarkup
)


def welcome_replies(message):
    markup = ReplyKeyboardMarkup(resize_keyboard=True)
    btn1 = types.KeyboardButton('🏠 Домой')
    btn2 = types.KeyboardButton('😌 Помощь')
    markup.row(btn1, btn2)
    return markup

    # bot.register_next_step_handler(message, on_click)

    # def on_click(message):
    #     if message.text == 'Перейти на наш сайт':
    #         bot.send_message(message.chat.id, 'Сайт открыт')
    #     elif message.text == 'Удалить фото':
    #         bot.send_message(message.chat.id, 'Delete')
