from telebot.types import (
    InlineKeyboardButton,
    InlineKeyboardMarkup,
)


def buy_button():
    keyboard = InlineKeyboardMarkup(row_width=2)
    keyboard.add(
        InlineKeyboardButton(text='Хочу купить 😍', url='https://wa.me/996705186262'),
        InlineKeyboardButton(text='↩️ Назад в меню', callback_data='back')
    )
    return keyboard

# def functions1():
#     keyboard = InlineKeyboardMarkup()
#     keyboard.add(
#         InlineKeyboardButton(text='🛍️ Перейти к покупкам', callback_data='buy'),
#         InlineKeyboardButton(text='💎 Перейти в наш телеграмм канал', url='https://t.me/beisheevaadelina')
#     )
#     return keyboard
