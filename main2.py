import telebot
from telebot import types
import random
from keyboards.inline import inlines
from keyboards.reply import replies
from telebot.types import (
    InlineKeyboardButton,
    InlineKeyboardMarkup,
    ReplyKeyboardMarkup
)

bot = telebot.TeleBot('6916461355:AAFWDNl78kbSjgAKeKB9ZKtY-MqHgyzK8gM')

answers = ['Я не понял, что ты хочешь сказать.', 'Извини, я тебя не понимаю.',
           'Я не знаю такой команды.', 'Мой разработчик не говорил, что отвечать в такой ситуации... >_<']


@bot.message_handler(commands=['start'])
def welcome(message):
    bot.send_message(chat_id=message.chat.id,
                     text=f'Привет, {message.from_user.first_name}!', reply_markup=replies.welcome_replies(message))

    if message.text == '/start':
        bot.send_message(chat_id=message.chat.id,
                         text='У меня ты сможешь купить некоторые товары!', reply_markup=functions1())

    # else:
    #     bot.send_message(message.chat.id, 'Понятий не имею что не понятного 🤷')


def functions1():
    keyboard = InlineKeyboardMarkup(row_width=1)
    keyboard.add(
        InlineKeyboardButton(text='🛍️ Перейти к покупкам', callback_data='buy'),
        InlineKeyboardButton(text='❗️ Написать нам по WhatsApp', url='https://wa.me/996705186262'),
        InlineKeyboardButton(text='💎 Перейти в наш телеграмм канал', url='https://t.me/+Ya4DHGACNt5kMGIy')
    )
    return keyboard


def buy_product_inline_keyboard():
    keyboard = InlineKeyboardMarkup(row_width=2)
    keyboard.add(
        InlineKeyboardButton(text='🍽️ Посуда для кухни', callback_data='option1'),
        InlineKeyboardButton(text='🫖 Чайники', callback_data='option2'),
        InlineKeyboardButton(text='🛋️ Пылесосы', callback_data='option3'),
        InlineKeyboardButton(text='🫕 Каструли', callback_data='option4'),
        InlineKeyboardButton(text='🥘 Казаны', callback_data='option5'),
        InlineKeyboardButton(text='🥟 Мантышницы', callback_data='option6'),
        InlineKeyboardButton(text='🍶 Термосы', callback_data='option7'),
        InlineKeyboardButton(text='🥵 Обогреватели', callback_data='option8'),
        InlineKeyboardButton(text='💇‍♀️ Что-то для волос', callback_data='option9'),
        InlineKeyboardButton(text='🪤 Еще что-то', callback_data='option10'),
        InlineKeyboardButton(text='↩️ Назад в меню', callback_data='back')
    )
    return keyboard


@bot.message_handler()
def handle_regular_messages(message):
    if message.text.lower() == 'buy':
        bot.send_message(chat_id=message.chat.id, text=f'Привет, {message.from_user.first_name}!', reply_markup=buy_product_inline_keyboard())
    else:
        bot.send_message(message.chat.id, answers[random.randint(0, 3)])


@bot.callback_query_handler(func=lambda call: True)
def handle_callback_query(call):
    if call.data == 'buy':
        bot.answer_callback_query(call.id)
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id, text=f'Привет, {call.from_user.first_name}!', reply_markup=buy_product_inline_keyboard())

    elif call.data == 'back':
        bot.answer_callback_query(call.id)
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id, text='Вы вернулись в главное меню!', reply_markup=functions1()),
    
    elif call.data == 'option1':
        fileBT1001 = open('./img_opn1/BT-1001.jpeg', 'rb')
        bot.send_photo(chat_id=call.message.chat.id, photo=fileBT1001)
        bot.send_message(chat_id=call.message.chat.id, text="6-размер 45с\n7-размер 50с\n8-размер 60с\n9-размер 80с\n10-размер 95с \n\n 🆔 Код товара: BT-1001", reply_markup=inlines.buy_button()),

        fileBT1002 = open('./img_opn1/BT-1002.jpeg', 'rb')
        bot.send_photo(chat_id=call.message.chat.id, photo=fileBT1002)
        bot.send_message(chat_id=call.message.chat.id, text="6-размер 55с\n7-размер 70с\n8-размер 80с\n9-размер 95с\n10-размер 140с \n\n 🆔 Код товара: BT-1002", reply_markup=inlines.buy_button()),

        fileBT1003 = open('./img_opn1/BT-1003.jpeg', 'rb')
        bot.send_photo(chat_id=call.message.chat.id, photo=fileBT1003)
        bot.send_message(chat_id=call.message.chat.id, text="6-размер 45с\n7-размер 60с\n8-размер 70с\n9-размер 85с\n10-размер 110с \n\n 🆔 Код товара: BT-1003", reply_markup=inlines.buy_button()),

        fileBT1004 = open('./img_opn1/BT-1004.jpeg', 'rb')
        bot.send_photo(chat_id=call.message.chat.id, photo=fileBT1004)
        bot.send_message(chat_id=call.message.chat.id, text="7-размер 85с\n8-размер 110с\n9-размер 150с \n\n 🆔 Код товара: BT-1004", reply_markup=inlines.buy_button()),

        fileBT1005 = open('./img_opn1/BT-1005.jpeg', 'rb')
        bot.send_photo(chat_id=call.message.chat.id, photo=fileBT1005)
        bot.send_message(chat_id=call.message.chat.id, text="8-размер 100с\n9-размер 120с \n\n 🆔 Код товара: BT-1005", reply_markup=inlines.buy_button()),

        fileBT1006 = open('./img_opn1/BT-1006.jpeg', 'rb')
        bot.send_photo(chat_id=call.message.chat.id, photo=fileBT1006)
        bot.send_message(chat_id=call.message.chat.id, text="8-размер 80с\n9-размер 110с \n\n 🆔 Код товара: BT-1006", reply_markup=inlines.buy_button()),

        fileBT1007 = open('./img_opn1/BT-1007.jpeg', 'rb')
        bot.send_photo(chat_id=call.message.chat.id, photo=fileBT1007)
        bot.send_message(chat_id=call.message.chat.id, text="8-размер 100с\n9-размер 115с \n\n 🆔 Код товара: BT-1007", reply_markup=inlines.buy_button()),

    elif call.data == 'option2':
        fileSK0808 = open('./img_opn2/SK-0808.jpeg', 'rb')
        bot.send_photo(chat_id=call.message.chat.id, photo=fileSK0808)
        bot.send_message(chat_id=call.message.chat.id, text="💵 Цена: 1000 сом  \n 🆔 Код товара: SK-0808", reply_markup=inlines.buy_button()),

        fileSKSH1077 = open('./img_opn2/SK-SH-1077.jpeg', 'rb')
        bot.send_photo(chat_id=call.message.chat.id, photo=fileSKSH1077)
        bot.send_message(chat_id=call.message.chat.id, text="💵 Цена: 1000 сом  \n 🆔 Код товара: SK-SH-1077", reply_markup=inlines.buy_button()),

    elif call.data == 'option3':
        fileSK0808 = open('./img_opn2/SK-0808.jpeg', 'rb')
        bot.send_photo(chat_id=call.message.chat.id, photo=fileSK0808)
        bot.send_message(chat_id=call.message.chat.id, text="💵 Цена: 1000 сом  \n 🆔 Код товара: SK-0808", reply_markup=inlines.buy_button()),

        fileSKSH1077 = open('./img_opn2/SK-SH-1077.jpeg', 'rb')
        bot.send_photo(chat_id=call.message.chat.id, photo=fileSKSH1077)
        bot.send_message(chat_id=call.message.chat.id, text="💵 Цена: 1000 сом  \n 🆔 Код товара: SK-SH-1077", reply_markup=inlines.buy_button()),

    elif call.data == 'option4':
        fileSK0808 = open('./img_opn2/SK-0808.jpeg', 'rb')
        bot.send_photo(chat_id=call.message.chat.id, photo=fileSK0808)
        bot.send_message(chat_id=call.message.chat.id, text="💵 Цена: 1000 сом  \n 🆔 Код товара: SK-0808", reply_markup=inlines.buy_button()),

        fileSKSH1077 = open('./img_opn2/SK-SH-1077.jpeg', 'rb')
        bot.send_photo(chat_id=call.message.chat.id, photo=fileSKSH1077)
        bot.send_message(chat_id=call.message.chat.id, text="💵 Цена: 1000 сом  \n 🆔 Код товара: SK-SH-1077", reply_markup=inlines.buy_button()),

    elif call.data == 'option5':
        fileSK0808 = open('./img_opn2/SK-0808.jpeg', 'rb')
        bot.send_photo(chat_id=call.message.chat.id, photo=fileSK0808)
        bot.send_message(chat_id=call.message.chat.id, text="💵 Цена: 1000 сом  \n 🆔 Код товара: SK-0808", reply_markup=inlines.buy_button()),

        fileSKSH1077 = open('./img_opn2/SK-SH-1077.jpeg', 'rb')
        bot.send_photo(chat_id=call.message.chat.id, photo=fileSKSH1077)
        bot.send_message(chat_id=call.message.chat.id, text="💵 Цена: 1000 сом  \n 🆔 Код товара: SK-SH-1077", reply_markup=inlines.buy_button()),

    elif call.data == 'option6':
        fileSK0808 = open('./img_opn2/SK-0808.jpeg', 'rb')
        bot.send_photo(chat_id=call.message.chat.id, photo=fileSK0808)
        bot.send_message(chat_id=call.message.chat.id, text="💵 Цена: 1000 сом  \n 🆔 Код товара: SK-0808", reply_markup=inlines.buy_button()),

        fileSKSH1077 = open('./img_opn2/SK-SH-1077.jpeg', 'rb')
        bot.send_photo(chat_id=call.message.chat.id, photo=fileSKSH1077)
        bot.send_message(chat_id=call.message.chat.id, text="💵 Цена: 1000 сом  \n 🆔 Код товара: SK-SH-1077", reply_markup=inlines.buy_button()),

    elif call.data == 'option7':
        fileSK0808 = open('./img_opn2/SK-0808.jpeg', 'rb')
        bot.send_photo(chat_id=call.message.chat.id, photo=fileSK0808)
        bot.send_message(chat_id=call.message.chat.id, text="💵 Цена: 1000 сом  \n 🆔 Код товара: SK-0808", reply_markup=inlines.buy_button()),

        fileSKSH1077 = open('./img_opn2/SK-SH-1077.jpeg', 'rb')
        bot.send_photo(chat_id=call.message.chat.id, photo=fileSKSH1077)
        bot.send_message(chat_id=call.message.chat.id, text="💵 Цена: 1000 сом  \n 🆔 Код товара: SK-SH-1077", reply_markup=inlines.buy_button()),

    elif call.data == 'option8':
        fileSK0808 = open('./img_opn2/SK-0808.jpeg', 'rb')
        bot.send_photo(chat_id=call.message.chat.id, photo=fileSK0808)
        bot.send_message(chat_id=call.message.chat.id, text="💵 Цена: 1000 сом  \n 🆔 Код товара: SK-0808", reply_markup=inlines.buy_button()),

        fileSKSH1077 = open('./img_opn2/SK-SH-1077.jpeg', 'rb')
        bot.send_photo(chat_id=call.message.chat.id, photo=fileSKSH1077)
        bot.send_message(chat_id=call.message.chat.id, text="💵 Цена: 1000 сом  \n 🆔 Код товара: SK-SH-1077", reply_markup=inlines.buy_button()),

    elif call.data == 'option9':
        fileSK0808 = open('./img_opn2/SK-0808.jpeg', 'rb')
        bot.send_photo(chat_id=call.message.chat.id, photo=fileSK0808)
        bot.send_message(chat_id=call.message.chat.id, text="💵 Цена: 1000 сом  \n 🆔 Код товара: SK-0808", reply_markup=inlines.buy_button()),

        fileSKSH1077 = open('./img_opn2/SK-SH-1077.jpeg', 'rb')
        bot.send_photo(chat_id=call.message.chat.id, photo=fileSKSH1077)
        bot.send_message(chat_id=call.message.chat.id, text="💵 Цена: 1000 сом  \n 🆔 Код товара: SK-SH-1077", reply_markup=inlines.buy_button()),

    elif call.data == 'option10':
        fileSK0808 = open('./img_opn2/SK-0808.jpeg', 'rb')
        bot.send_photo(chat_id=call.message.chat.id, photo=fileSK0808)
        bot.send_message(chat_id=call.message.chat.id, text="💵 Цена: 1000 сом  \n 🆔 Код товара: SK-0808", reply_markup=inlines.buy_button()),

        fileSKSH1077 = open('./img_opn2/SK-SH-1077.jpeg', 'rb')
        bot.send_photo(chat_id=call.message.chat.id, photo=fileSKSH1077)
        bot.send_message(chat_id=call.message.chat.id, text="💵 Цена: 1000 сом  \n 🆔 Код товара: SK-SH-1077", reply_markup=inlines.buy_button()),


bot.polling(non_stop=True)
